FROM golang AS build

WORKDIR /go/src/github.com/balena-io-projects/app

COPY . ./

# RUN go mod init

RUN go build

FROM golang

COPY --from=build /go/src/github.com/balena-io-projects/app/ .

CMD ./go_base_edge_node
