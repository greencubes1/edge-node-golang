package main

import (
	"fmt"
	"log"
	"os/exec"
	"time"

	"github.com/go-co-op/gocron"
)

func call_os_pwd() {
	timestamp := time.Now().UTC()
	picFilename :=  "/data/" + timestamp.String() + ".png"
	println(picFilename)
	args := []string{"-i", "/dev/video0", "-vframes", "1", picFilename}
	_, err := exec.Command("ffmpeg", args...).Output()
	if err != nil {
		log.Fatal(err)
	}
	// fmt.Println(string(out))
	fmt.Println(picFilename)
}
var task = func() {call_os_pwd()}

func main() {
	s := gocron.NewScheduler(time.UTC)
	_, _ = s.Every(60).Second().Do(task)
	// _, _ = s.Every(1 * time.Second).Do(task)
	// _, _ = s.Every("1s").Do(task)
	// s.StartAsync()
	s.StartBlocking()
}